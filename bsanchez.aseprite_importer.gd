tool
extends EditorPlugin

var import_plugin = null

func get_name():
  return "Aseprite Importer"

func _enter_tree():
  _start()

func _exit_tree():
  _stop()

func _start():
  if import_plugin == null:
    import_plugin = preload('aseprite_importer.gd').new()
    add_import_plugin(import_plugin)

func _stop():
  if import_plugin != null:
    import_plugin = null
    remove_import_plugin(import_plugin)
