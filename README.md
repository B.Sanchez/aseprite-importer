# Aseprite Importer
> Import easily your Aseprites files in Godot without losing custom tracks.

## Installation

### Manually

Clone the repository and put it in your addons folder.

### As a git submodule

```sh
$ git submodule add https://gitlab.com/B.Sanchez/aseprite-importer.git addons/bsanchez.aseprite_importer
```

## Usage

### Basic usage (no track additions/modifications)

Export your sprite sheet from Aseprite (File/Export Sprite Sheet).Enable Meta: Frame Tags, and be sure to export .json and .png files in the same directory under your gosot project.
Inside Godot, you can now use the json file like an AnimationPlayer.

Simply repeay the last export to update the AnimationPlayer and Sprite (no Godot manipulations needed)

### With tracks addition/modifications

Follow the steps from the basic usage, then make the AsepriteAnimation local (right click/make local). 
Then select the .json file, open importer panel, select the .tscn corresponding to your scene and hit "Reimport".

You can now add tracks and edit all the non-imported tracks.

## Release History

* 0.1.0
    * The job is done, but code smell is present and some features are left unimplemented

## License

BSanchez – contact@bsanchez.fr

Distributed under the MIT license. See ``LICENSE`` for more information.

## Contributing

2. Create your feature branch (`git checkout -b feature/fooBar`)
3. Commit your changes (`git commit -am 'Add some fooBar'`)
4. Push to the branch (`git push origin feature/fooBar`)
5. Create a new Pull Request
