# TODO
# 1/ Gestion des params de tracks
# 5/ On gère l'export avec aseprite directement

tool
extends EditorImportPlugin

const AsepriteParser = preload("./AsepriteParser.gd")

# TODO: , PRESET_ANIMATED_SPRITE, PRESET_ATLAS
enum Presets { ANIMATION_PLAYER }

func get_importer_name() -> String:
  return "bsanchez.aseprite_importer"

func get_visible_name() -> String:
  return "Aseprite Importer"

func get_recognized_extensions() -> Array:
  return ["json"] # .ase, .aseprite

func get_save_extension() -> String:
  return "scn"
  
func get_resource_type() -> String:
  return "PackedScene"

func get_option_visibility(option, options) -> bool:
  return true

func get_preset_count() -> int:
  return Presets.size()

func get_preset_name(preset: int) -> String:
  match preset:
    Presets.ANIMATION_PLAYER: return "Animation Player"
    _: return "Unnamed preset"

# A mettre dans un fichier spécifique avec des enum et tout
func get_import_options(preset: int) -> Array:
  # TODO Ajouter un liev vers le dossier des textures
  var options =  [
    {
      name = "scene",
      default_value = "",
      property_hint = PROPERTY_HINT_FILE,
      hint_string = "*.tscn",
    },
    {
      name = "animation_player_name",
      default_value = "AnimationPlayer",
    }
  ]
  return options

func import(resource_path: String, import_path: String, import_options, r_platform_variants, r_gen_files) -> int:
  var error;
  var json_path = resource_path
  
  var aseprite_parser: AsepriteParser = AsepriteParser.new()
  error = aseprite_parser.from_json( json_path,  json_path.get_base_dir() + "/" )  
  if error:
    # TODO Un message d'erreur
    return error
    
  var scene_imported: Node = aseprite_parser.scene
  var packed_scene_imported: PackedScene = PackedScene.new()
  packed_scene_imported.pack(scene_imported)
  error = ResourceSaver.save( import_path + "." + get_save_extension(), packed_scene_imported )
  if error:
    # TODO Un message d'erreur
    return error

  #############################################################
  # On a pas a aller plus loin, et du couop ça c'est bien passé
  if !import_options.scene:
    return OK
    
  var file: File = File.new()
  if file.file_exists( import_options.scene ):
    var animation_player_imported = scene_imported.find_node('AnimationPlayer')
    var animation_names = animation_player_imported.get_animation_list()
    
    var packed_scene_permanent: PackedScene = load(import_options.scene)
    var scene_permanent = packed_scene_permanent.instance()
    var animation_player_permanent: AnimationPlayer = scene_permanent.find_node(import_options.animation_player_name)
    
    for animation_name in animation_names:
      var animation_imported = animation_player_imported.get_animation(animation_name)
      if animation_player_permanent.has_animation(animation_name):
        var animation_permanent = animation_player_permanent.get_animation(animation_name)

        for track_id_imported in animation_imported.get_track_count ():
          var track_path = animation_imported.track_get_path(track_id_imported)
          var track_id_permanent = animation_permanent.find_track(track_path)

          # TODO tracker des paramètres interessants depuis track_id_permanent
          animation_imported.track_set_imported(track_id_imported, false)
          animation_imported.track_set_enabled(track_id_imported, animation_permanent.track_is_enabled(track_id_permanent))
          
          if track_id_permanent != -1:
            animation_permanent.remove_track(track_id_permanent)

          animation_imported.copy_track(track_id_imported, animation_permanent)
      else:
        animation_player_permanent.add_animation(animation_name, Animation.new())
        
      packed_scene_permanent.pack(scene_permanent)
      error = ResourceSaver.save( import_options.scene, packed_scene_permanent )
      if error:
        # TODO Un message d'erreur
        return error
  return OK
