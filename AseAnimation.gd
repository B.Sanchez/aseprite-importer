# Class AseAnimation
tool

const AseFrame = preload("./AseFrame.gd")

enum DIRECTION { FORWARD, REVERSE, PINGPONG }
var name: String
var length: float = 0.0 setget disable_setter,get_length
var _frames: Array = []
var frames: Array = [] setget disable_setter,get_frames
var direction: int = 0 setget set_direction

func _init(_name: String):
  self.name = _name  

func get_length() -> float:
  if length == 0.0:
    length = 0.0
    for _frame in _frames:
      length += _frame.duration
  return length

func get_frames() -> Array:
  if  frames == []:
    var _reverse_frames: Array = _frames.duplicate()
    _reverse_frames.invert()
    match direction:
      DIRECTION.FORWARD: frames = _frames
      DIRECTION.REVERSE: frames = _reverse_frames
      DIRECTION.PINGPONG: frames = _frames + _reverse_frames
  return frames
  
func get_frame(index: int) -> AseFrame:
  return self.frames[index]
  
func disable_setter(unused) -> void:
  pass
  
func set_direction(new_direction: int) -> void:
  direction = new_direction
  frames = []

func add_frame(frame: AseFrame):
  _frames.append(frame)
  length = 0.0
  frames = []
