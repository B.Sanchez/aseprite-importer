# Class AsepriteParser
tool

const AseFrame = preload("./AseFrame.gd")
const AseAnimation = preload("./AseAnimation.gd")

enum FORMAT { HASH, ARRAY }
const TRACK_NAME = "Sprite:region_rect"

# ----- ERROR HANDLING -----
enum ERROR { FILE_OPEN, PARSING_JSON, MALFORMATED_ASEPRITE_DATA, WRONG_EXPORT_OPTIONS, 
TEXTURE_FILE_NOT_FOUND, TEXTURE_FILE_NOT_A_TEXTURE }
func print_error_message(error_id: int, args: Array = []) -> void:
  match error_id:
    ERROR.FILE_OPEN: print("Failed to open JSON file \"%s\" (code %s)" % args)
    ERROR.PARSING_JSON: print("%s is not a valid JSON file" % args)
    ERROR.MALFORMATED_ASEPRITE_DATA: print("%s is not a valid aseprite export" % args)
    ERROR.WRONG_EXPORT_OPTIONS: print("Remember to check \"Frame tags\" when exporting" % args)
    ERROR.TEXTURE_FILE_NOT_FOUND: print("%s file not found" % args)
    ERROR.TEXTURE_FILE_NOT_A_TEXTURE: print("%s file is not a valide texture file type" % args)
    _: print("Error message unavailable")
# ----- /ERROR HANDLING -----

var json_file_path: String
var json_file_content: String
var aseprite_data: Dictionary
var export_format: int
var frames_list: Array = []
var animations: Dictionary = {}
var animations_names: Array = []
var texture_path: String
var texture: Texture
var scene: AsepriteAnimation = AsepriteAnimation.new()

# ----- Public -----

# TODO le texture_dir_path
func from_json(json_path: String, texture_dir_path: String = "res://") -> int:
  var error: int
  json_file_path = json_path

  error = _read_json_file()
  if error: return error
  
  error = _parse_json()
  if error: return error
  
  # TODO a faire sauter si on a déjà un sc
  error = _find_sprite_path(texture_dir_path)
  if error: return error  
  
  error = _detect_export_type()
  if error: return error  
  
  match export_format:
    FORMAT.ARRAY: error = _init_frames_from_array(aseprite_data.frames)
    FORMAT.HASH: error = _init_frames_from_dictionary(aseprite_data.frames)
  if error: return error
  
  error = _init_animations()
  if error: return error
  
  error = _make_sprite()
  if error: return error
  
  error = _make_animation_player()
  if error: return error
  
  return OK

func get_animations_names() -> Array:
  return animations_names
  
func get_animation(animation_name) -> Animation:
  if animations.has(animation_name):
    return animations[animation_name]
  else:
    return null

func get_frame(index: int) -> AseFrame:
  return self.frames_list[index] 

# ----- Private -----
    
func _read_json_file() -> int:
  var file: File = File.new()
  var error: int = file.open( json_file_path, File.READ )
  if error:
    print_error_message(ERROR.FILE_OPEN, [json_file_path, error])
  else:
    self.json_file_content = file.get_as_text()
  return error # or OK, vu que si error = 0, on vaut OK
 
func _parse_json() -> int:
  var temp_data: Dictionary = parse_json(json_file_content)
  if temp_data == null || typeof(temp_data) != TYPE_DICTIONARY:
    print_error_message(ERROR.PARSING_JSON, [self.json_file_path, FAILED])
    return FAILED
  else:
    aseprite_data = temp_data
    return OK
    
func _find_sprite_path(texture_dir_path: String) -> int:
  if !aseprite_data.has("meta"):
    print_error_message(ERROR.MALFORMATED_ASEPRITE_DATA, [self.json_file_path])
    return ERR_INVALID_DATA
    
  texture_path = texture_dir_path + aseprite_data.meta.image
  
  var file: File = File.new()
  if not file.file_exists( texture_path ):
    print_error_message(ERROR.TEXTURE_FILE_NOT_FOUND, [texture_path])
    return ERR_FILE_NOT_FOUND
    
  texture = load( texture_path )
  if not typeof(texture) == TYPE_OBJECT or not texture is Texture:
    print_error_message(ERROR.TEXTURE_FILE_NOT_A_TEXTURE, [texture_path])
    return ERR_INVALID_DATA  
    
  return OK

func _detect_export_type() -> int:
  match typeof(aseprite_data.frames):
    TYPE_DICTIONARY: export_format = FORMAT.HASH
    TYPE_ARRAY: export_format = FORMAT.ARRAY
    _:
      print_error_message(ERROR.MALFORMATED_ASEPRITE_DATA, [self.json_file_path])
      return ERR_INVALID_DATA
  return OK

func _init_frames_from_array(frames: Array) -> int:
  for frame_data in frames:
    if !(frame_data.has("duration") && frame_data.has("frame")):
      print_error_message(ERROR.MALFORMATED_ASEPRITE_DATA, [self.json_file_path])
      return ERR_INVALID_DATA
      
    self.frames_list.append(AseFrame.new(frame_data.frame, frame_data.duration))
  return OK
  
func _init_frames_from_dictionary (frames: Dictionary) -> int:
  return _init_frames_from_array(frames.values()) 
  
func _init_animations() -> int:
  if !aseprite_data.has("meta"):
    print_error_message(ERROR.MALFORMATED_ASEPRITE_DATA, [self.json_file_path])
    return ERR_INVALID_DATA
  
  if !aseprite_data.meta.has("frameTags"):
    print_error_message(ERROR.WRONG_EXPORT_OPTIONS)
    return ERR_INVALID_DATA
  
  for animation_data in aseprite_data.meta.frameTags:
    var animation: AseAnimation = AseAnimation.new(animation_data.name)
    
    match animation_data.direction:
      "forward": animation.direction = AseAnimation.DIRECTION.FORWARD
      "reverse": animation.direction = AseAnimation.DIRECTION.REVERSE
      "pingpong": animation.direction = AseAnimation.DIRECTION.PINGPONG
      _:
        print_error_message(ERROR.MALFORMATED_ASEPRITE_DATA, [self.json_file_path])
        return ERR_INVALID_DATA
    
    for i in range( animation_data.from, animation_data.to + 1 ):

      # Si l'index est plus grand que la taille du tableau, c'est qu'on a un soucis
      if self.frames_list.size() - 1 < i:
        print_error_message(ERROR.MALFORMATED_ASEPRITE_DATA, [self.json_file_path])
        return ERR_INVALID_DATA
        
      animation.add_frame(self.frames_list[i])
    
    self.animations_names.append (animation_data.name)
    self.animations[animation_data.name] = animation

  return OK

func _make_sprite() -> int:
  var sprite = Sprite.new()
  sprite.set_texture( texture )
  sprite.set_region_rect( self.get_frame(0).rect )
  sprite.set_region( true )
  
  scene.add_child(sprite)
  sprite.owner = scene
  
  return OK

func _make_animation_player() -> int:
  var animation_player: AnimationPlayer = AnimationPlayer.new()
  scene.add_child(animation_player)
  animation_player.owner = scene

  # Création des animations
  for animation_name in self.animations_names:
    var ase_animation = self.get_animation(animation_name)
    var animation = Animation.new()
    
    # Crétation de la track
    var track_id: int = animation.add_track( Animation.TYPE_VALUE, 0 )
    animation.track_set_path( track_id, TRACK_NAME )
    animation.track_set_interpolation_type( track_id, Animation.INTERPOLATION_NEAREST )
    animation.value_track_set_update_mode( track_id, Animation.UPDATE_DISCRETE )
    animation.set_loop( false )
    
    # Création des clés
    var next_key_time: float = 0.0
    for frame in ase_animation.frames:
      animation.track_insert_key( track_id, next_key_time, frame.rect )
      next_key_time += frame.duration

    animation.length = ase_animation.length
    animation_player.add_animation( animation_name, animation )
  
  return OK
