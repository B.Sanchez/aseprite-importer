# Class AseFrame
tool

var rect: Rect2
var duration: float
func _init(_rect: Dictionary, _duration: int) -> void:
  self.rect = Rect2( _rect.x, _rect.y, _rect.w, _rect.h )
  self.duration = float(_duration) / 1000
